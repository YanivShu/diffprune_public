import abc
from dataclasses import dataclass
from typing import Optional, List, Tuple, Union

import tensorflow as tf

from layers import DiffPruneActivation, DiffPruneLayerBase, DiffPruneResidualBlock


@dataclass
class Config:
    """
    Contains model hyper-parameters configuration.
    """

    prune: List[bool]
    use_regularizer: List[bool]
    sigmoid_gates: List[bool]
    lambda_: List[float]
    eta: List[Optional[float]]
    train_eta: List[bool]
    dtype: tf.DType = tf.float32


class ModelBase(tf.keras.Model, abc.ABC):
    """
    Common functions for all models used in this module.
    """

    def __init__(
        self, dtype: Optional[tf.DType] = None, name: Optional[str] = None, **kwargs
    ) -> None:
        """
        Args:
            dtype: The dtype for the model, layers and outputs.
            name: Model's name.
            **kwargs: Additional arguments for base tf.keras.Model.
        """
        super(ModelBase, self).__init__(dtype=dtype, name=name, **kwargs)
        self._classification_loss_value: tf.Tensor = tf.convert_to_tensor(
            value=0, dtype=dtype
        )
        self._classification_loss: tf.keras.losses.SparseCategoricalCrossentropy = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=True
        )
        self._inputs_shape: List[tf.TensorShape] = []

    def loss(self, y_true: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
        """
        Calculates total loss for mini-batch using cross-entropy loss for predictions and regularization losses.
        Args:
            y_true: Sparse class labels for each instance in the mini-batch.
            y_pred: Class logits for each instance in the mini-batch.

        Returns:
            Total mini-batch loss.
        """
        self._classification_loss_value = self._classification_loss(
            y_true=y_true, y_pred=y_pred
        )
        regularization_losses: List[tf.Tensor] = []

        for l in self.layers:
            if isinstance(l, DiffPruneLayerBase) or isinstance(
                l, DiffPruneResidualBlock
            ):
                regularization_losses += l.losses

        return (
            tf.reduce_mean(input_tensor=regularization_losses)
            if len(regularization_losses) > 0
            else 0.0
        ) + self._classification_loss_value

    def write_summaries(self, step: int, training: bool) -> None:
        """
        Writes model's specific TensorBoard summaries.
        Args:
            step: The training/test step.
            training: True when training.
        """
        tf.summary.scalar(
            name="classification_loss", data=self._classification_loss_value, step=step
        )
        flops: tf.Tensor = self.count_flops(training=training)
        tf.summary.scalar(name="total_flops", data=flops, step=step)

        for l in self.layers:
            if isinstance(l, DiffPruneLayerBase) or isinstance(
                l, DiffPruneResidualBlock
            ):
                l.write_summaries(step=step)

    def on_epoch_end(self, **kwargs) -> None:
        """
        Writes epoch summaries and calls DiffPrune layers' on_epoch_end methods.
        Args:
            kwargs: Expecting 'training' set to True when training and False in inference 'epoch' the current epoch
            number. Can contain additional arguments for DiffPrune layers' on_epoch_end.
        """
        training: bool = kwargs["training"]
        epoch: int = kwargs["epoch"]
        flops: tf.Tensor = self.count_flops(training=training)
        tf.summary.scalar(name="total_flops_epoch", data=flops, step=epoch)

        for l in self.layers:
            if isinstance(l, DiffPruneLayerBase) or isinstance(
                l, DiffPruneResidualBlock
            ):
                l.on_epoch_end(**kwargs)

    def call(
        self, inputs: tf.Tensor, training: bool = False, **kwargs
    ) -> Tuple[tf.Tensor, tf.GradientTape]:
        """
        Forward propagation.
        Args:
            inputs: Mini-batch of data instances.
            training: True when training, False when in inference.
            **kwargs: Optional additional parameters for layers call.

        Returns:
            Prediction logits and the GradientTape instance.
        """
        update_shapes = len(self._inputs_shape) == 0

        with tf.GradientTape() as tape:
            x: tf.Tensor = inputs
            for l in self._my_layers:
                if update_shapes:
                    self._inputs_shape.append(x.shape)
                x = l(inputs=x, training=training)

        if update_shapes:
            self._inputs_shape.append(x.shape)

        return x, tape

    @abc.abstractmethod
    def count_flops(self, training: bool) -> tf.Tensor:
        """
        Args:
            training: True when training, False when in inference.

        Returns:
            The approximate FLOP count for the forward pass considering the convolution, dense and ops used for pruning.
        """
        pass


class LeNet5ActivationPrune(ModelBase):
    """
    An implementation of the basic convolutional network LeNet5 with added DiffPrune layers for pruning Conv2D filters
    and inputs to the Dense layers.
    """

    def __init__(
        self,
        prune: List[bool],
        use_regularizer: List[bool],
        sigmoid_gates: List[bool],
        lambda_: List[float],
        eta: List[Optional[float]],
        train_eta: List[bool],
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        Lists correspond to the DiffPrune layer by order.
        Args:
            prune: True to prune the inputs to the DiffPrune layer, False to use the full architecture.
            use_regularizer: True to use L0 regularization, False to train with no regularization.
            sigmoid_gates: True to use u(*) := sigmoid(*), False to use u(*) := softmax(*).
            lambda_: Scalers for the L0 loss.
            eta: If specified then Gaussian dropout with p = sigmoid(eta) is applied. If None dropout is not applied.
            train_eta: True to train the dropout rate, False to keep it constant.
            dtype: The dtype for the model, layers and outputs.
            name: Model's name.
            **kwargs: Additional arguments for super class.
        """
        super(LeNet5ActivationPrune, self).__init__(dtype=dtype, name=name, **kwargs)
        self._my_layers: List[tf.keras.layers.Layer] = []
        self._my_layers.append(tf.keras.layers.BatchNormalization())
        self._my_layers.append(
            tf.keras.layers.Conv2D(
                filters=20, kernel_size=(5, 5), activation="relu", name="conv_1"
            )
        )
        if prune[0]:
            self._my_layers.append(
                DiffPruneActivation(
                    axis=-1,
                    lambda_=lambda_[0],
                    use_regularizer=use_regularizer[0],
                    sigmoid_gates=sigmoid_gates[0],
                    eta=eta[0],
                    train_eta=train_eta[0],
                    dtype=dtype,
                    name="diffprune_0_conv_1",
                    **kwargs,
                )
            )
        self._my_layers.append(tf.keras.layers.MaxPooling2D())

        self._my_layers.append(
            tf.keras.layers.Conv2D(
                filters=50, kernel_size=(5, 5), activation="relu", name="conv_2"
            )
        )
        if prune[1]:
            self._my_layers.append(
                DiffPruneActivation(
                    axis=-1,
                    lambda_=lambda_[1],
                    use_regularizer=use_regularizer[1],
                    sigmoid_gates=sigmoid_gates[1],
                    eta=eta[1],
                    train_eta=train_eta[1],
                    dtype=dtype,
                    name="diffprune_1_conv_2",
                    **kwargs,
                )
            )
        self._my_layers.append(tf.keras.layers.MaxPooling2D())

        self._my_layers.append(tf.keras.layers.Flatten())

        if prune[2]:
            self._my_layers.append(
                DiffPruneActivation(
                    lambda_=lambda_[2],
                    use_regularizer=use_regularizer[2],
                    sigmoid_gates=sigmoid_gates[2],
                    eta=eta[2],
                    train_eta=train_eta[2],
                    dtype=dtype,
                    name="diffprune_2_flatten",
                    **kwargs,
                )
            )

        self._my_layers.append(
            tf.keras.layers.Dense(units=500, activation="relu", name="dense")
        )

        if prune[3]:
            self._my_layers.append(
                DiffPruneActivation(
                    lambda_=lambda_[3],
                    use_regularizer=use_regularizer[3],
                    sigmoid_gates=sigmoid_gates[3],
                    eta=eta[3],
                    train_eta=train_eta[3],
                    dtype=dtype,
                    name="diffprune_3_dense",
                    **kwargs,
                )
            )

        self._my_layers.append(
            tf.keras.layers.Dense(units=10, activation="linear", name="output")
        )

    def count_flops(self, training: bool) -> tf.Tensor:
        """
        Args:
            training: Should be set to True when training.

        Returns:
            The expected number of flops performed by the convolution, dot product and pruning operations in the
            forward pass.
        """
        flops: List[tf.Tensor] = []
        prev_layer: Optional[tf.keras.layers.Layer] = None
        next_layer: Optional[tf.keras.layers.Layer] = None

        for i, l in enumerate(self._my_layers):
            if i < len(self._my_layers) - 1:
                next_layer: tf.keras.layers.Layer = self._my_layers[i + 1]

            if isinstance(l, tf.keras.layers.Conv2D):
                num_kernel_params: int = l.kernel_size[0] * l.kernel_size[
                    1
                ] * self._inputs_shape[i][-1]
                num_ops_activation: int = 2 * num_kernel_params  # N mul + N - 1 add + 1 add bias
                num_activations: tf.Tensor = tf.cast(
                    tf.reduce_prod(self._inputs_shape[i + 1]), dtype=self.dtype
                )
                flops.append(num_ops_activation * num_activations)  # ops * activations

            elif isinstance(l, DiffPruneActivation) and isinstance(
                prev_layer, tf.keras.layers.Conv2D
            ):
                flops[-1] = flops[-1] * tf.reduce_mean(l.last_binarized_z)
                flops.append(l.count_flops(training=training))

            elif isinstance(l, DiffPruneActivation) and isinstance(
                next_layer, tf.keras.layers.Dense
            ):
                num_ops_activation = (
                    2 * self._inputs_shape[i + 1][-1]
                )  # N mul + N - 1 add + 1 add bias
                keep_rate: tf.Tensor = tf.reduce_mean(l.last_binarized_z)
                flops.append(
                    num_ops_activation
                    * keep_rate
                    * tf.cast(
                        tf.reduce_prod(self._inputs_shape[i + 1]), dtype=self.dtype
                    )
                )
                flops.append(l.count_flops(training=training))

            prev_layer = l

        return tf.reduce_sum(flops)


class WideResidualNetworkDiffPrune(ModelBase):
    """
    An implementation of the WRN-28-10 Wide Residual Network with added DiffPrune layers for pruning Conv2D filters.
    See http://www.bmva.org/bmvc/2016/papers/paper087/paper087.pdf.
    """

    def __init__(
        self,
        num_classes: int,
        depth: int,
        width: int,
        prune: List[bool],
        use_regularizer: List[bool],
        sigmoid_gates: List[bool],
        lambda_: List[float],
        eta: List[Optional[float]],
        train_eta: List[bool],
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        Lists correspond to the DiffPrune layer by order.
        Args:
            num_classes: The number of classes in the dataset.
            depth: The WRN depth.
            width: The WRN width.
            prune: True to prune the inputs to the DiffPrune layer, False to use the full architecture.
            use_regularizer: True to use L0 regularization, False to train with no regularization.
            sigmoid_gates: True to use u(*) := sigmoid(*), False to use u(*) := softmax(*).
            lambda_: Scalers for the L0 loss.
            eta: If specified then Gaussian dropout with p = sigmoid(eta) is applied. If None dropout is not applied.
            train_eta: True to train the dropout rate, False to keep it constant.
            dtype: The dtype for the model, layers and outputs.
            name: Model's name.
            **kwargs: Additional arguments for super class.
        """
        assert (depth - 4) % 6 == 0
        super(WideResidualNetworkDiffPrune, self).__init__(
            dtype=dtype, name=name, **kwargs
        )

        residual_blocks_per_group: int = (depth - 4) // 6
        filters_in_group: List[int] = [x * width for x in [16, 32, 64]]

        assert len(prune) == residual_blocks_per_group * 3

        self._my_layers: List[Union[tf.keras.layers.Layer, tf.keras.Model]] = []

        # standardize the inputs
        self._my_layers.append(tf.keras.layers.BatchNormalization())

        self._my_layers.append(
            tf.keras.layers.Conv2D(
                filters=16,
                kernel_size=(3, 3),
                padding="same",
                activation="linear",
                use_bias=False,
                name="conv_1",
            )
        )

        for g in range(3):
            for i in range(residual_blocks_per_group):
                conf_offset: int = g * residual_blocks_per_group + i
                self._my_layers.append(
                    DiffPruneResidualBlock(
                        filters=filters_in_group[g],
                        conv_shortcut=i == 0,
                        down_sample=g > 0 and i == 0,
                        prune=prune[conf_offset],
                        use_regularizer=use_regularizer[conf_offset],
                        sigmoid_gates=sigmoid_gates[conf_offset],
                        lambda_=lambda_[conf_offset],
                        eta=eta[conf_offset],
                        train_eta=train_eta[conf_offset],
                        dtype=dtype,
                        name=f"diffPrune_res_block_group_{g}_block_{i}",
                    )
                )

        self._my_layers.append(tf.keras.layers.BatchNormalization())
        self._my_layers.append(tf.keras.layers.Activation("relu"))
        self._my_layers.append(tf.keras.layers.AveragePooling2D(pool_size=(8, 8)))
        self._my_layers.append(tf.keras.layers.Flatten())
        self._my_layers.append(
            tf.keras.layers.Dense(units=num_classes, activation="linear")
        )

    def count_flops(self, training: bool) -> tf.Tensor:
        """
        Args:
            training: Should be set to True when training, False for inference.

        Returns:
            The expected number of flops performed by the convolution, dense and pruning operations in the forward pass.
        """
        flops: List[tf.Tensor] = []
        for i, l in enumerate(self._my_layers):
            if isinstance(l, tf.keras.layers.Conv2D):
                num_kernel_params: int = l.kernel_size[0] * l.kernel_size[
                    1
                ] * self._inputs_shape[i][-1]
                num_ops_activation: int = 2 * num_kernel_params - 1  # N mul + N - 1 add
                num_activations: tf.Tensor = tf.cast(
                    tf.reduce_prod(self._inputs_shape[i + 1]), dtype=self.dtype
                )
                flops.append(num_ops_activation * num_activations)  # ops * activations

            elif isinstance(l, DiffPruneResidualBlock):
                flops.append(l.count_flops(training=training))

            elif isinstance(l, tf.keras.layers.Dense):
                num_ops_activation = (
                    2 * self._inputs_shape[i][-1]
                )  # N mul + N - 1 add + 1 add bias
                flops.append(
                    num_ops_activation
                    * tf.cast(
                        tf.reduce_prod(self._inputs_shape[i + 1]), dtype=self.dtype
                    )
                )

        return tf.reduce_sum(flops)
