from pathlib import Path
from typing import List, Dict

import tensorflow as tf

from data.common import load_mnist
from models import LeNet5ActivationPrune, Config
from run import train_and_test


def main() -> None:
    batch_size: int = 100

    tests: Dict[str, Config] = dict(
        orig=Config(
            prune=[False] * 4,
            use_regularizer=[False] * 4,
            sigmoid_gates=[False] * 4,
            lambda_=[0.0] * 4,
            eta=[None] * 4,
            train_eta=[False] * 4,
        ),
        no_reg_loss_eta_trained_softmax=Config(
            prune=[True] * 4,
            use_regularizer=[False] * 4,
            sigmoid_gates=[False] * 4,
            lambda_=[0.0] * 4,
            eta=[0.0] * 4,
            train_eta=[True] * 4,
        ),
        l0_reg_mixed_lambda_eta_trained_sigmoid=Config(
            prune=[True] * 4,
            use_regularizer=[True] * 4,
            sigmoid_gates=[True] * 4,
            lambda_=[1e-5, 1e-5, 2e-5, 2e-5],
            eta=[-1.7339] * 4,
            train_eta=[True] * 4,
        ),
    )

    suite: List[str] = [k for k in tests.keys()]

    tests_dir: Path = Path(__file__).parent.joinpath(
        "out", "lenet5_activation_prune_mnist"
    )
    num_epochs: int = 201

    for test in suite:
        conf: Config = tests[test]
        tf.keras.backend.set_floatx(str(conf.dtype).split("'")[1])
        ds_train, ds_test = load_mnist(batch_size=batch_size, dtype=conf.dtype)

        model_dir: Path = tests_dir.joinpath(test)
        print(f"Starting test {test}")

        model: LeNet5ActivationPrune = LeNet5ActivationPrune(
            prune=conf.prune,
            use_regularizer=conf.use_regularizer,
            sigmoid_gates=conf.sigmoid_gates,
            lambda_=conf.lambda_,
            eta=conf.eta,
            train_eta=conf.train_eta,
            dtype=conf.dtype,
        )

        train_and_test(
            model=model,
            train_dataset=ds_train,
            test_dataset=ds_test,
            optimizer=tf.keras.optimizers.Adam(learning_rate=5e-4),
            num_epochs=num_epochs,
            model_dir=model_dir,
            summary_steps=50,
            max_trained_epochs=num_epochs,
            max_to_keep=20,
        )

        print(f"Finished test {test}")
        tf.keras.backend.clear_session()


if __name__ == "__main__":
    tf.compat.v1.enable_eager_execution()
    tf.config.experimental_run_functions_eagerly(run_eagerly=True)
    main()
