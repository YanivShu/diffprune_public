from pathlib import Path
from typing import Tuple

import tensorflow as tf
import tensorflow_datasets as tfds

tf_data_dir: Path = Path(__file__).parent.absolute().joinpath("tensorflow_datasets")


def load_mnist(
    batch_size: int, dtype: tf.DType
) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
    """
    Args:
        batch_size: The batch size for both train and test datasets.
        dtype: The data type for the datasets.

    Returns:
        the MNIST datasets for training and testing
    """
    (ds_train, ds_test), ds_info = tfds.load(
        name="mnist",
        split=["train", "test"],
        data_dir=tf_data_dir,
        shuffle_files=True,
        as_supervised=True,
        with_info=True,
    )
    normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(
        1.0 / 255
    )
    ds_train = ds_train.map(
        lambda x, y: (
            tf.cast(normalization_layer(x), dtype=dtype),
            tf.cast(y, dtype=dtype),
        ),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )
    ds_train = ds_train.cache()
    ds_train = ds_train.shuffle(ds_info.splits["train"].num_examples)
    ds_train = ds_train.batch(batch_size)
    ds_train = ds_train.prefetch(tf.data.experimental.AUTOTUNE)
    ds_test = ds_test.map(
        lambda x, y: (
            tf.cast(normalization_layer(x), dtype=dtype),
            tf.cast(y, dtype=dtype),
        ),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )
    ds_test = ds_test.batch(batch_size)
    ds_test = ds_test.cache()
    ds_test = ds_test.prefetch(tf.data.experimental.AUTOTUNE)
    return ds_train, ds_test


def load_cifar10(
    batch_size: int, dtype: tf.DType
) -> Tuple[tf.data.Dataset, tf.data.Dataset, tfds.core.DatasetInfo]:
    """
    Args:
        batch_size: The batch size for both train and test datasets.
        dtype: The data type for the datasets.

    Returns:
        The CIFAR10 datasets for training and testing and the dataset information.
    """
    (ds_train, ds_test), ds_info = tfds.load(
        name="cifar10",
        split=["train", "test"],
        data_dir=tf_data_dir,
        shuffle_files=True,
        as_supervised=True,
        with_info=True,
    )

    normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(
        1.0 / 255
    )

    pre_process: tf.keras.Sequential = tf.keras.Sequential(
        [
            tf.keras.layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
            tf.keras.layers.experimental.preprocessing.RandomTranslation(
                height_factor=0.12, width_factor=0.12, fill_mode="reflect"
            ),
        ]
    )

    ds_train = ds_train.map(
        lambda x, y: (normalization_layer(x), y),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )
    ds_train = ds_train.shuffle(ds_info.splits["train"].num_examples)
    ds_train = ds_train.batch(batch_size)
    ds_train = ds_train.map(
        lambda x, y: (
            tf.cast(pre_process(x, training=True), dtype=dtype),
            tf.cast(y, dtype=dtype),
        ),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )
    ds_train = ds_train.prefetch(tf.data.experimental.AUTOTUNE)

    ds_test = ds_test.map(
        lambda x, y: (
            tf.cast(normalization_layer(x), dtype=dtype),
            tf.cast(y, dtype=dtype),
        ),
        num_parallel_calls=tf.data.experimental.AUTOTUNE,
    )
    ds_test = ds_test.batch(batch_size)
    ds_test = ds_test.prefetch(tf.data.experimental.AUTOTUNE)
    return ds_train, ds_test, ds_info
