import abc
from typing import Optional, Union, Dict, List

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow_probability as tfp


class DiffPruneLayerBase(abc.ABC, keras.layers.Layer):
    def __init__(
        self,
        beta: Optional[float],
        lambda_: float,
        use_regularizer: bool,
        sigmoid_gates: bool,
        eta: Optional[float],
        train_eta: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        Args:
            beta: Threshold selection for the importance probability where weights having an importance probability less
            than beta are dropped. If None it is automatically estimated from the initialized values of u(mu).
            lambda_: Scaler for the L0 loss.
            use_regularizer: True to use L0 regularization, False to prune without regularization.
            sigmoid_gates: True to use u(*) := sigmoid(*), False to use u(*) := softmax(*).
            eta: If specified then Gaussian dropout with p = sigmoid(eta) is applied. If None dropout is not applied.
            train_eta: True to train the dropout rate, False to keep in constant.
            dtype: The dtype for the layer and all weights and variables.
            name: Optional name for the layer.
            **kwargs: Additional parameters for keras.layers.Layer.
        """
        super(DiffPruneLayerBase, self).__init__(name=name, dtype=dtype, **kwargs)
        self._use_regularizer: bool = use_regularizer
        self._sigmoid_gates: bool = sigmoid_gates

        self._beta: tf.Variable = tf.Variable(
            initial_value=0.0,
            trainable=False,
            name=f"{self.name}/beta",
            dtype=self.dtype,
        )

        self._estimate_beta: bool = True

        if beta is not None:
            self._estimate_beta: bool = False
            self._beta.assign(value=beta)

        self._lambda: tf.Tensor = tf.constant(
            value=lambda_, name="lambda", dtype=self.dtype
        )

        self._eta: Optional[tf.Variable] = None

        if eta is not None:
            self._eta = tf.Variable(
                initial_value=eta,
                trainable=train_eta,
                name=f"{self.name}/eta",
                dtype=self.dtype,
            )

        self._zeta: tf.Variable = tf.Variable(
            initial_value=0.0,
            trainable=True,
            name=f"{self.name}/zeta",
            dtype=self.dtype,
            constraint=tf.keras.constraints.NonNeg(),
        )

        self._mu: Optional[tf.Variable] = None
        self._last_z: Optional[tf.Tensor] = None
        self.last_binarized_z: Optional[tf.Tensor] = None

    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Initializes beta if required.
        Args:
            input_shape: The shape of the inputs.
        """
        if self._estimate_beta:
            p: tf.Tensor = self._importance_probs(logits=self._mu)
            self._beta.assign(0.99 * tf.reduce_min(p))

    def _importance_probs(self, logits: Union[tf.Tensor, tf.Variable]) -> tf.Tensor:
        """
        Returns:
            The importance probability for the sparsity partition.
        """
        if self._sigmoid_gates:
            return tf.keras.activations.sigmoid(logits)
        else:
            return tf.keras.activations.softmax(logits)

    def _drop_rate(self) -> tf.Tensor:
        """
        Returns:
            The drop rate for the Gaussian dropout.
        """
        p: tf.Tensor = tf.keras.activations.sigmoid(self._eta)
        return tf.sqrt(p / (1.0 - p))

    def _calculate_gates(self, probs: tf.Tensor) -> tf.Tensor:
        """
        Args:
            probs: The importance probability distribution over the weights.
        Returns:
            The approximate binary gates.
        """
        z: tf.Tensor = tf.keras.activations.relu(probs - self._beta)
        where_z_plus: tf.Tensor = tf.not_equal(z, 0.0)
        z_plus: tf.Tensor = (
            z[where_z_plus] - tf.reduce_mean(z[where_z_plus])
        ) * tf.exp(-self._zeta) + 1.0
        z = tf.tensor_scatter_nd_update(
            tensor=z, indices=tf.where(where_z_plus), updates=z_plus
        )
        self._last_z = z
        self.last_binarized_z = tf.cast(tf.greater(z, 0.0), dtype=z.dtype)
        return z

    def _loss(self) -> tf.Tensor:
        """
        Returns:
            The L0 regularization loss for the layer. A constant 0 is returned if regularization is not used.
        """
        if not self._use_regularizer:
            return tf.constant(value=0.0, dtype=self.dtype)

        if self._sigmoid_gates:
            return self._reg_l0_sigmoid_loss()
        else:
            return self._reg_l0_softmax_loss()

    def _reg_l0_sigmoid_loss(self) -> tf.Tensor:
        """
        Returns:
            The L0 regularization loss for sparsity partition with u(*) := sigmoid(mu).
        """
        return self._lambda * tf.reduce_sum(
            1.0
            - tfp.distributions.Normal(loc=self._mu, scale=1.0).cdf(
                -tf.math.log(1 / self._beta - 1)
            )
        )

    def _reg_l0_softmax_loss(self) -> tf.Tensor:
        """
        Returns:
            The L0 regularization loss for sparsity partition with u(*) := softmax(mu).
        """
        exp_mu: tf.Tensor = tf.exp(self._mu)
        mus = tf.reduce_sum(exp_mu)
        mus: tf.Tensor = tf.tile(
            tf.expand_dims(mus, axis=0), multiples=[self._mu.shape[-1]]
        )
        mus -= exp_mu

        return self._lambda * tf.reduce_sum(
            1.0
            - tfp.distributions.Normal(loc=self._mu, scale=1.0).cdf(
                tf.math.log(self._beta / (1 - self._beta) * mus)
            )
        )

    def write_summaries(self, step: int) -> None:
        """
        Writes loss and parameter statistics to TensorBoard summaries.
        Args:
            step: The training/validation step
        """
        if len(self.losses) > 0:
            tf.summary.scalar(
                name=f"{self._name}/diffprune_reg_loss", data=self.losses[0], step=step
            )

        tf.summary.histogram(name=f"{self.name}/z", data=self._last_z, step=step)

        tf.summary.scalar(
            name=f"{self.name}/active_gates",
            data=tf.reduce_sum(self.last_binarized_z),
            step=step,
        )

        tf.summary.scalar(
            name=f"{self._name}/dropped_ratio",
            data=1.0 - tf.reduce_mean(self.last_binarized_z),
            step=step,
        )

    def on_epoch_end(self, **kwargs) -> None:
        """
        Writes end of epoch summaries.
        Args:
            kwargs: Should contain "epoch" the current epoch number.
        """
        epoch: tf.Tensor = kwargs["epoch"]

        tf.summary.scalar(
            name=f"{self.name}/active_gates_epoch",
            data=tf.reduce_sum(self.last_binarized_z),
            step=epoch,
        )

        tf.summary.scalar(
            name=f"{self._name}/dropped_ratio_epoch",
            data=1.0 - tf.reduce_mean(self.last_binarized_z),
            step=epoch,
        )

        tf.summary.scalar(name=f"{self._name}/beta_epoch", data=self._beta, step=epoch)

        where_z_plus: tf.Tensor = tf.not_equal(self._last_z, 0.0)

        tf.summary.scalar(
            name=f"{self._name}/enabled_gates_std_epoch",
            data=tf.math.reduce_std(self._last_z[where_z_plus]),
            step=epoch,
        )

        tf.summary.scalar(
            name=f"{self._name}/enabled_gates_max_epoch",
            data=tf.reduce_max(self._last_z[where_z_plus]),
            step=epoch,
        )

        tf.summary.scalar(
            name=f"{self._name}/enabled_gates_min_epoch",
            data=tf.reduce_min(self._last_z[where_z_plus]),
            step=epoch,
        )

        if self._eta is not None:
            tf.summary.scalar(
                name=f"{self.name}/drop_rate_epoch", data=self._drop_rate(), step=epoch
            )

    def count_flops(self, training: bool) -> tf.Tensor:
        """
        Count flops for calculating gates. Assuming sampling is 5 ops for sample. exp, sqrt and sigmoid are 10 ops.
        Logical and floating point arithmetics are 1 op.
        Args:
            training: True when training.

        Returns:
            Expected number of flops performed by the layer.
        """
        gate_ops: tf.Tensor = (
            self.last_binarized_z.shape[-1] * (1 + 1 + 1 + 1)
            + tf.reduce_mean(self.last_binarized_z) * (1 + 1 + 1)
            + 10  # tf.exp(-self._zeta)
        )

        drop_ops: tf.Tensor = tf.constant(0.0, dtype=self.dtype)
        if self._eta is not None and training:
            drop_ops = self.last_binarized_z.shape[-1] * (10 + 1 + 1 + 10 + 5 + 1)

        return gate_ops + drop_ops

    @abc.abstractmethod
    def call(self, inputs: tf.Tensor, **kwargs) -> tf.Tensor:
        pass


class DiffPruneActivation(DiffPruneLayerBase):
    def __init__(
        self,
        axis: Optional[int] = None,
        beta: Optional[float] = None,
        lambda_: float = 1.0,
        use_regularizer: bool = False,
        sigmoid_gates: bool = True,
        eta: Optional[float] = None,
        train_eta: bool = True,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        Args:
            axis: Specifies the axis gates are applied to. Must be none or -1 currently as it was tested only with these
            values. A value of None treats all the the inputs as a single sparsity group. A value of -1 used with the
            activations of a Conv2D layer with channels last format will result in filter pruning.
            beta: Threshold selection for the importance  probability where weights having a probability less than beta
            are dropped. If None it is automatically estimated from the initialized values of u(mu).
            lambda_: Scaler for the L0 loss.
            use_regularizer: True to use L0 regularization, False to prune without regularization.
            eta: If specified then Gaussian dropout with p = sigmoid(eta) is applied. If None dropout is not applied.
            train_eta: True to train the dropout rate, False to keep in constant.
            dtype: The dtype for the layer and all weights and variables.
            name: Optional name for the layer.
            **kwargs: Additional parameters for keras.layers.Layer.
        """
        assert axis is None or axis == -1

        super(DiffPruneActivation, self).__init__(
            beta=beta,
            lambda_=lambda_,
            use_regularizer=use_regularizer,
            sigmoid_gates=sigmoid_gates,
            eta=eta,
            train_eta=train_eta,
            dtype=dtype,
            name=name,
            **kwargs,
        )

        self._axis: Optional[int] = axis

    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Creates the specific variables of the layer. Assumes shape[0] is batch size.
        Args:
            input_shape: The shape of the inputs.
        """
        initializer: tf.keras.initializers.TruncatedNormal = tf.keras.initializers.TruncatedNormal()

        if self._axis is None:
            values: tf.Tensor = initializer(
                shape=(1, tf.reduce_prod(input_shape[1:])), dtype=self.dtype
            )
        else:
            values: tf.Tensor = initializer(
                shape=(1, input_shape[self._axis]), dtype=self.dtype
            )

        self._mu = tf.Variable(initial_value=values, trainable=True, name="mu")
        super(DiffPruneActivation, self).build(input_shape=input_shape)

    def call(self, inputs: tf.Tensor, **kwargs) -> tf.Tensor:
        """
        Layer's forward pass. Assumes inputs.shape[0] is batch size.
        Args:
            inputs: The activations of the pruned layer.
            kwargs: Requires the boolean argument 'training' set to True when training and False when in inference.

        Returns: The pruned (gated) activations.
        """
        training: bool = kwargs["training"]
        mu = self._mu.read_value()

        if self._eta is not None and training:
            mu *= tfp.distributions.Normal(loc=1.0, scale=self._drop_rate()).sample(
                sample_shape=mu.shape
            )

        probs: tf.Tensor = self._importance_probs(logits=mu)
        z: tf.Tensor = self._calculate_gates(probs=probs)
        self.add_loss(losses=self._loss())
        z = tf.squeeze(input=z)

        if self._axis is None:
            z = tf.reshape(z, shape=inputs.shape[1:])

        return inputs * z


class DiffPruneResidualBlock(tf.keras.Model):
    """
       Implementation of the B(3,3) block of the wide residual network with filter pruning applied to the hidden Conv2D
       layer and joint pruning of the second Conv2D layer and the skip connection. The joint pruning is applied to
       the results of the addition of the inputs with the output of the seconds conv layer to enforce equal
       dimensions and alignment. See http://www.bmva.org/bmvc/2016/papers/paper087/paper087.pdf.
        """

    def __init__(
        self,
        filters: int,
        conv_shortcut: bool,
        down_sample: bool,
        prune: bool,
        use_regularizer: bool,
        sigmoid_gates: bool,
        lambda_: float,
        eta: Optional[float],
        train_eta: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> None:
        """
        DiffPrune parameter value are used for all DiffPrune layers.
        Args:
            filters: The number of filters in each of the Conv2D layers.
            conv_shortcut: True to pass the inputs through a Conv2D layer.
            down_sample: True to down sample by half the spatial dimensionality of the inputs.
            prune: True to apply pruning, False to act as a standard B(3,3) block.
            use_regularizer: True to use L0 regularization in the DiffPrune layers, False to prune with no
            regularization.
            lambda_: Scaler for the L0 loss, applied to all DiffPrune layers.
            eta: If specified then Gaussian dropout with p = sigmoid(eta) is applied. If None dropout is not applied.
            train_eta: True to train the dropout rate, False to keep in constant. The same value is used for all
            DiffPrune layers.
            dtype: The dtype for the block, all weights, variables and outputs.
            name: Block's name.
            **kwargs: Additional arguments for base tf.keras.Model.
        """
        super(DiffPruneResidualBlock, self).__init__(dtype=dtype, name=name, **kwargs)

        self._inputs_shape: Dict[str, tf.TensorShape] = {}
        self._my_layers: List[tf.keras.layers.Layer] = []

        self._my_layers.append(tf.keras.layers.BatchNormalization(name="bn_1"))
        self._my_layers.append(tf.keras.layers.Activation("relu", name="relu_1"))
        self._my_layers.append(
            tf.keras.layers.Conv2D(
                filters=filters,
                kernel_size=(3, 3),
                strides=(2, 2) if down_sample else (1, 1),
                padding="same",
                activation="linear",
                use_bias=False,
                name="conv_1",
            )
        )

        self._conv_1_index: int = len(self.layers) - 1
        self._my_layers.append(tf.keras.layers.BatchNormalization(name="bn_2"))
        self._my_layers.append(tf.keras.layers.Activation("relu", name="relu_2"))

        self._diffprune_conv_1_index: Optional[int] = None

        if prune:
            self._my_layers.append(
                DiffPruneActivation(
                    axis=-1,
                    lambda_=lambda_,
                    use_regularizer=use_regularizer,
                    sigmoid_gates=sigmoid_gates,
                    eta=eta,
                    train_eta=train_eta,
                    dtype=dtype,
                    name=f"{self.name}/diffprune_conv_1",
                    **kwargs,
                )
            )

            self._diffprune_conv_1_index = len(self.layers) - 1

        self._my_layers.append(
            tf.keras.layers.Conv2D(
                filters=filters,
                kernel_size=(3, 3),
                padding="same",
                activation="linear",
                use_bias=False,
                name="conv_2",
            )
        )

        self._conv_2_index: int = len(self.layers) - 1

        self._conv_shortcut: Optional[tf.keras.layers.Conv2D] = None

        if conv_shortcut:
            self._conv_shortcut = tf.keras.layers.Conv2D(
                filters=filters,
                kernel_size=(1, 1),
                strides=(2, 2) if down_sample else (1, 1),
                padding="valid" if down_sample else "same",
                activation="linear",
                use_bias=False,
                name="conv_shortcut",
            )
            self._conv_shortcut_index: int = len(self.layers) - 1

        self._output_diffprune: Optional[DiffPruneActivation] = None

        if prune:
            self._output_diffprune = DiffPruneActivation(
                axis=-1,
                lambda_=lambda_,
                use_regularizer=use_regularizer,
                sigmoid_gates=sigmoid_gates,
                eta=eta,
                train_eta=train_eta,
                dtype=dtype,
                name=f"{self.name}/diffprune_output",
                **kwargs,
            )

    def loss(self) -> List[tf.Tensor]:
        """
        Returns:
            Sum of L0 regularization mini-batch loss for all DiffPrune layers in the block.
        """
        loss: tf.Tensor = tf.constant(0.0, dtype=self.dtype)

        if self._diffprune_conv_1_index is not None:
            loss += tf.reduce_sum(self.layers[self._diffprune_conv_1_index].losses)

        if self._output_diffprune is not None:
            loss += tf.reduce_sum(self._output_diffprune.losses)

        return loss

    def call(self, inputs: tf.Tensor, training: bool = False, **kwargs) -> tf.Tensor:
        """
        Forward propagation.
        Args:
            inputs: Mini-batch of activations.
            training: True when training, False when in inference.
            **kwargs: Optional additional parameters for layers call.
        Returns:
            Activations for this block.
        """
        update_shapes = len(self._inputs_shape) == 0

        x: tf.Tensor = inputs
        for l in self._my_layers:
            if update_shapes:
                self._inputs_shape[l.name] = x.shape
            x = l(inputs=x, training=training)

        if self._conv_shortcut is not None:
            if update_shapes:
                self._inputs_shape[self._conv_shortcut.name] = inputs.shape

            inputs = self._conv_shortcut(inputs=inputs, training=training)

        if update_shapes:
            self._inputs_shape["output"] = x.shape

        output: tf.Tensor = x + inputs

        if self._output_diffprune is not None:
            output = self._output_diffprune(output)

        return output

    def write_summaries(self, step: int) -> None:
        """
        Writes layer's specific TensorBoard summaries.
        Args:
            step: The training/test step
        """
        for l in self.layers:
            if isinstance(l, DiffPruneLayerBase):
                l.write_summaries(step=step)

    def on_epoch_end(self, **kwargs) -> None:
        """
        Calls DiffPrune layers' on_epoch_end method.
        """
        for l in self.layers:
            if isinstance(l, DiffPruneLayerBase):
                l.on_epoch_end(**kwargs)

    def count_flops(self, training: bool) -> tf.Tensor:
        """
        Calculates the total number of flops performed by the convolution, dense and pruning layers.
        Args:
            training:
        """

        def conv2d_flops(conv_idx: int, next_layer_name: str) -> tf.Tensor:
            conv_2d: tf.keras.layers.Conv2D = self.layers[conv_idx]
            num_kernel_params: int = (
                conv_2d.kernel_size[0]
                * conv_2d.kernel_size[1]
                * self._inputs_shape[conv_2d.name][-1]
            )
            num_ops_activation: int = 2 * num_kernel_params - 1  # N mul + N - 1 add
            num_activations: tf.Tensor = tf.cast(
                tf.reduce_prod(self._inputs_shape[next_layer_name]), dtype=self.dtype
            )
            return num_ops_activation * num_activations

        flops: List[tf.Tensor] = [
            conv2d_flops(conv_idx=self._conv_1_index, next_layer_name="bn_2")
        ]

        if self._diffprune_conv_1_index is not None:
            diffprune_conv_1: DiffPruneActivation = self.layers[
                self._diffprune_conv_1_index
            ]
            flops[-1] = flops[-1] * tf.reduce_mean(diffprune_conv_1.last_binarized_z)
            flops.append(diffprune_conv_1.count_flops(training=training))

        flops.append(
            conv2d_flops(conv_idx=self._conv_2_index, next_layer_name="output")
        )

        if self._output_diffprune is not None:
            flops[-1] = flops[-1] * tf.reduce_mean(
                self._output_diffprune.last_binarized_z
            )
            flops.append(self._output_diffprune.count_flops(training=training))

        if self._conv_shortcut is not None:
            flops.append(
                conv2d_flops(
                    conv_idx=self._conv_shortcut_index, next_layer_name="output"
                )
            )
            if self._output_diffprune is not None:
                flops[-1] = flops[-1] * tf.reduce_mean(
                    self._output_diffprune.last_binarized_z
                )

        return tf.reduce_sum(flops)
