import time
from pathlib import Path
from typing import Iterable, Tuple, Optional, List, Any

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow_addons.optimizers import SGDW


class Sgdw(SGDW):
    """
    implementation of the weight decay decoupled SGD that does not perform weight decay om the diffprune_conv layers of
    the wide ResNet.
    """

    def _decay_weights_op(self, var, apply_state=None):
        if "/mu:0" in var.name:
            return tf.no_op()

        if "/eta:0" in var.name:
            return tf.no_op()

        if "/zeta:0" in var.name:
            return tf.no_op()

        return super(Sgdw, self)._decay_weights_op(var=var, apply_state=apply_state)


def _loss_and_gradients(
    model: keras.Model,
    y_true: tf.Tensor,
    y_pred: tf.Tensor,
    tape: tf.GradientTape,
    is_training: bool,
) -> Tuple[tf.Tensor, Optional[List[tf.Tensor]]]:
    """
    Calculates loss as well as gradients when training for all related tensors.
    Args:
        model: The model.
        y_true: The labels or target outputs.
        y_pred: Model's predictions.
        tape: The GradientTape used in forward propagation.
        is_training: True if training, otherwise False.

    Returns:
        The total step loss and gradients when training.
    """
    gradients: Optional[List[tf.Tensor]] = None
    with tape:
        step_loss: tf.Tensor = model.loss(y_true=y_true, y_pred=y_pred)

    if is_training:
        gradients = tape.gradient(step_loss, model.trainable_variables)

    return step_loss, gradients


def _test_train_loop(
    is_training: bool,
    model: keras.Model,
    dataset: Iterable[Tuple[Any, ...]],
    optimizer: Optional[keras.optimizers.Optimizer],
    num_epochs: int,
    model_dir: Path,
    summary_steps: Optional[int],
    checkpoint_steps: Optional[int],
    max_trained_epochs: Optional[int] = None,
    max_to_keep: int = 10,
) -> tf.Variable:
    """
    Restores model parameters from last checkpoint if found and executes forward and optionally backward propagation on
    the model. Writes checkpoints as training progresses and writes TensorBoard summaries.
    Args:
        is_training: True when training otherwise False.
        model: The model.
        dataset: An Iterable that produces Tuples in the form (x, y) where x is the inputs to the model and y is a
        tensor of labels.
        optimizer: The optimizer used to calculate gradients in the backward propagation step.
        num_epochs: Number of epochs to train or test.
        model_dir: The directory where checkpoints and summaries are output to.
        summary_steps: Number of steps between subsequent summary outputs are written.
        checkpoint_steps: Number of training steps between subsequent checkpoints are written. Irrespectively of this
        parameter the model outputs a checkpoint file at the end of each epoch.
        max_trained_epochs: Limit the absolute number of epochs trained.
        max_to_keep: The number of checkpoints file to keep by rotation.
    Returns:
        The number of total trained epochs for the model.
    """
    trained_epochs: tf.Variable = tf.Variable(initial_value=0, dtype=tf.int64)

    checkpoint_manager, summary_writer, status = _files_and_checkpoints(
        is_training=is_training,
        model=model,
        model_dir=model_dir,
        optimizer=optimizer,
        trained_epochs=trained_epochs,
        max_to_keep=max_to_keep,
    )

    if max_trained_epochs is not None and tf.greater_equal(
        trained_epochs, max_trained_epochs
    ):
        status.expect_partial()
        return trained_epochs

    epoch_mean_total_loss: tf.keras.metrics.Mean = tf.keras.metrics.Mean()
    epoch_mean_accuracy: tf.keras.metrics.SparseCategoricalAccuracy = (
        tf.keras.metrics.SparseCategoricalAccuracy()
    )
    epoch_mean_eps: tf.keras.metrics.Mean = tf.keras.metrics.Mean()
    summary_steps_mean_total_loss: tf.keras.metrics.Mean = tf.keras.metrics.Mean()
    summary_steps_mean_eps: tf.keras.metrics.Mean = tf.keras.metrics.Mean()
    summary_steps_accuracy: tf.keras.metrics.SparseCategoricalAccuracy = (
        tf.keras.metrics.SparseCategoricalAccuracy()
    )

    step: int = 0

    start: float = time.time()
    for epoch in range(num_epochs):
        for x, y_true in dataset:
            y_pred, tape = model(
                x, step=step, summary_steps=summary_steps, training=is_training
            )

            write_loss_summaries: bool = (
                summary_steps is not None and step % summary_steps == 0
            )

            total_loss, gradients = _loss_and_gradients(
                model=model,
                y_pred=y_pred,
                y_true=y_true,
                tape=tape,
                is_training=is_training,
            )

            if gradients is not None:
                optimizer.apply_gradients(zip(gradients, model.trainable_variables))

            summary_steps_mean_total_loss(total_loss)
            summary_steps_accuracy(y_true, y_pred)

            step = optimizer.iterations.numpy() if is_training else step + 1

            end: float = time.time()
            eps: float = y_pred.shape[0] / (end - start)
            start = time.time()

            epoch_mean_total_loss(total_loss)
            epoch_mean_accuracy(y_true, y_pred)
            epoch_mean_eps(eps)
            summary_steps_mean_eps(eps)

            if is_training:
                if write_loss_summaries:
                    _write_summaries(
                        model=model,
                        step=step,
                        summary_steps=summary_steps,
                        summary_steps_accuracy=summary_steps_accuracy,
                        summary_steps_mean_eps=summary_steps_mean_eps,
                        summary_steps_mean_total_loss=summary_steps_mean_total_loss,
                        trained_epochs=trained_epochs,
                        y_pred=y_pred,
                    )

                if step % checkpoint_steps == 0:
                    save_path = checkpoint_manager.save()
                    print(f"Saved checkpoint for step {step}: {save_path}")

        if is_training:
            trained_epochs.assign_add(1)

        tf.summary.scalar(
            "epoch_mean_total_loss", epoch_mean_total_loss.result(), trained_epochs
        )
        tf.summary.scalar(
            "epoch_mean_accuracy", epoch_mean_accuracy.result(), trained_epochs
        )
        tf.summary.scalar("epoch_mean_eps", epoch_mean_eps.result(), trained_epochs)

        summary_writer.flush()

        epoch_mean_total_loss.reset_states()
        epoch_mean_accuracy.reset_states()
        epoch_mean_eps.reset_states()

        model.on_epoch_end(training=is_training, epoch=trained_epochs)

        if checkpoint_manager.latest_checkpoint:
            if is_training:
                status.assert_consumed()
            else:
                status.expect_partial()

        if is_training:
            save_path = checkpoint_manager.save()
            print(
                f"Saved checkpoint end of epoch {trained_epochs.numpy() - 1}, step {step}: {save_path}"
            )
            if max_trained_epochs is not None and tf.greater(
                trained_epochs, max_trained_epochs
            ):
                break

    summary_writer.close()
    return trained_epochs


def _write_summaries(
    model: keras.Model,
    step: int,
    summary_steps: int,
    summary_steps_accuracy: tf.metrics.SparseCategoricalAccuracy,
    summary_steps_mean_eps: tf.metrics.Mean,
    summary_steps_mean_total_loss: tf.metrics.Mean,
    trained_epochs: tf.Variable,
    y_pred: tf.Tensor,
) -> None:
    """
    Write metric summaries and reset metrics objects state.
    Args:
        model: The model.
        step: The current training/test step.
        summary_steps: Number of steps between subsequent summary outputs are written.
        summary_steps_accuracy: Mean accuracy since the last time summaries were written.
        summary_steps_mean_eps: Mean examples/second since the last time summaries were written.
        summary_steps_mean_total_loss: Mean batch loss since the last time summaries were written.
        trained_epochs: The current training epoch.
        y_pred: the last step's predictions generated by the model.
    """
    print(
        f"Epoch {trained_epochs.numpy()}, step {step}. Mean loss is {summary_steps_mean_total_loss.result()} "
        f"mean accuracy is {summary_steps_accuracy.result()} and over last {summary_steps}. "
        f"{summary_steps_mean_eps.result()} examples per second."
    )
    tf.summary.scalar("total loss", summary_steps_mean_total_loss.result(), step)
    tf.summary.scalar("accuracy", summary_steps_accuracy.result(), step)
    tf.summary.scalar(name="eps", data=summary_steps_mean_eps.result(), step=step)
    summary_steps_mean_total_loss.reset_states()
    summary_steps_accuracy.reset_states()
    summary_steps_mean_eps.reset_states()
    model.write_summaries(step=step, training=True)
    tf.summary.histogram(
        name="class prediction", data=tf.argmax(input=y_pred, axis=1), step=step
    )

    for v in model.trainable_variables:
        if tf.rank(v) > 0:
            tf.summary.scalar(name=f"{v.name} mean", data=tf.reduce_mean(v), step=step)
            tf.summary.scalar(name=f"{v.name} max", data=tf.reduce_max(v), step=step)
            tf.summary.scalar(name=f"{v.name} min", data=tf.reduce_min(v), step=step)
            tf.summary.scalar(
                name=f"{v.name} std", data=tf.math.reduce_std(v), step=step
            )
            tf.summary.histogram(name=v.name, data=v, step=step)
        else:
            tf.summary.scalar(name=v.name, data=v, step=step)


def _files_and_checkpoints(
    is_training: bool,
    model: keras.Model,
    model_dir: Path,
    optimizer: keras.optimizers.Optimizer,
    trained_epochs: tf.Variable,
    max_to_keep: int = 10,
) -> Tuple[tf.train.CheckpointManager, tf.summary.SummaryWriter, Any]:
    """
    Creates the model dir if doesn't exist. If the model dir contains any preexisting checkpoints the parameters are
    restored from the latest checkpoint file. If there are no checkpoints and the model is tested an exception is
    raised.
    Args:
        is_training: True when training, otherwise False.
        model: The model.
        model_dir: The directory where checkpoint and summary files are to be located.
        optimizer: The optimizer used to calculate gradients in the backward propagation step.
        trained_epochs: The current training epoch.
        max_to_keep: The number of checkpoints file to keep by rotation.

    Returns:
        The CheckpointManager, SummaryWriter and load status object.
    """
    summaries_dir: Path = model_dir.joinpath(
        "summaries", f'{"training" if is_training else "test"}'
    ).absolute()
    checkpoint_dir: Path = model_dir.joinpath("checkpoints").absolute()

    if not summaries_dir.is_dir():
        summaries_dir.mkdir(parents=True)
    if not checkpoint_dir.is_dir():
        checkpoint_dir.mkdir(parents=True)

    if is_training:
        checkpoint: tf.train.Checkpoint = tf.train.Checkpoint(
            optimizer=optimizer,
            model=model,
            trained_epochs=trained_epochs,
            **{v.name: v for v in model.non_trainable_variables},
        )
    else:
        checkpoint: tf.train.Checkpoint = tf.train.Checkpoint(
            model=model,
            trained_epochs=trained_epochs,
            **{v.name: v for v in model.non_trainable_variables},
        )

    checkpoint_manager = tf.train.CheckpointManager(
        checkpoint=checkpoint,
        directory=checkpoint_dir,
        max_to_keep=max_to_keep,
        keep_checkpoint_every_n_hours=1,
    )

    status = checkpoint.restore(checkpoint_manager.latest_checkpoint)

    if checkpoint_manager.latest_checkpoint:
        print(
            f"Restored from {checkpoint_manager.latest_checkpoint} is training {is_training}"
        )
    elif is_training:
        print(
            f"Initializing training from scratch. Saving checkpoints to {checkpoint_dir}"
        )
    else:
        raise FileNotFoundError(
            "No checkpoint found in {checkpoint_dir}, can't test model"
        )

    summary_writer: tf.summary.SummaryWriter = tf.summary.create_file_writer(
        logdir=str(summaries_dir)
    )
    summary_writer.set_as_default()

    return checkpoint_manager, summary_writer, status


@tf.function
def train(
    model: keras.Model,
    train_dataset: Iterable[Tuple[Any, ...]],
    optimizer: keras.optimizers.Optimizer,
    num_epochs: int,
    model_dir: Path,
    summary_steps: int = 10,
    checkpoint_steps: int = 1000,
    max_trained_epochs: Optional[int] = None,
    max_to_keep: int = 10,
) -> tf.Variable:
    """
    Trains the model. If checkpoints are present in the model dir then training resumes from the latest checkpoint.
    Args:
        model: The model.
        train_dataset: An Iterable that produces Tuples in the form (x, y) where x is the inputs to the model and y a
        tensor of labels.
        optimizer: The optimizer used to calculate gradients in the backward propagation step.
        num_epochs: Number of epochs to train.
        model_dir: The directory where checkpoint and summary files are to be located.
        summary_steps: Number of steps between subsequent summary outputs are written.
        checkpoint_steps: Number of training steps between subsequent checkpoints are written. Irrespectively of this
        value checkpoint are stored at the completion of every epoch.
        max_trained_epochs: Limit the absolute number of epochs trained.
        max_to_keep: The number of checkpoints file to keep by rotation.

    Returns:
        The number of total trained epochs for the model.
    """
    return _test_train_loop(
        is_training=True,
        model=model,
        dataset=train_dataset,
        optimizer=optimizer,
        num_epochs=num_epochs,
        model_dir=model_dir,
        summary_steps=summary_steps,
        checkpoint_steps=checkpoint_steps,
        max_trained_epochs=max_trained_epochs,
        max_to_keep=max_to_keep,
    )


@tf.function
def test(
    model: keras.Model,
    test_dataset: Iterable[Tuple[Any, ...]],
    model_dir: Path,
    summary_steps: int = 10,
    max_trained_epochs: Optional[int] = None,
) -> None:
    """
    Test (validate) the model. The model is restored from the latest checkpoint present in the model dir.
    Args:
        model: The model.
        test_dataset: An Iterable that produces Tuples in the form (x, y) where x is the inputs to the model and y is
        a tensor of labels.
        model_dir: The directory where checkpoint and summary files are to be located.
        summary_steps: Number of steps between subsequent summary outputs are written.
        max_trained_epochs: Limit the absolute number of epochs trained.
    """
    _test_train_loop(
        is_training=False,
        model=model,
        dataset=test_dataset,
        optimizer=None,
        num_epochs=1,
        model_dir=model_dir,
        summary_steps=summary_steps,
        checkpoint_steps=None,
        max_trained_epochs=max_trained_epochs,
    )


def train_and_test(
    model: keras.Model,
    train_dataset: Iterable[Tuple[Any, ...]],
    test_dataset: Iterable[Tuple[Any, ...]],
    optimizer: keras.optimizers.Optimizer,
    num_epochs: int,
    model_dir: Path,
    summary_steps: int = 10,
    checkpoint_steps: int = 1000,
    max_trained_epochs: Optional[int] = None,
    max_to_keep: int = 10,
) -> None:
    """
    Trains the model using the training set and runs a validation accuracy test on the validation set at the end of each
    training epoch.
    Args:
        model: The model.
        train_dataset: An Iterable that produces Tuples in the form (x, y) where x is the inputs to the model and y is
        a tensor of labels. This data is used only for training.
        test_dataset: An Iterable that produces Tuples in the form (x, y) where x is the inputs to the model and y is
        a tensor of labels. This data is used only for validation.
        optimizer: The optimizer used to calculate gradients in the backward propagation step.
        num_epochs: Number of epochs to train.
        model_dir: The directory where checkpoint and summary files are to be located.
        summary_steps: Number of steps between subsequent summary outputs are written.
        checkpoint_steps: Number of training steps between subsequent checkpoints are written. Irrespectively of this
        value checkpoint are stored at the completion of every epoch.
        max_trained_epochs: Limit the absolute number of epochs trained.
        max_to_keep: The number of checkpoints file to keep by rotation.
    """
    for i in range(num_epochs):
        trained_epochs: tf.Variable = train(
            model=model,
            train_dataset=train_dataset,
            optimizer=optimizer,
            num_epochs=1,
            model_dir=model_dir,
            summary_steps=summary_steps,
            checkpoint_steps=checkpoint_steps,
            max_trained_epochs=max_trained_epochs,
            max_to_keep=max_to_keep,
        )

        test(
            model=model,
            test_dataset=test_dataset,
            model_dir=model_dir,
            summary_steps=summary_steps,
            max_trained_epochs=max_trained_epochs,
        )

        if max_trained_epochs is not None and tf.greater_equal(
            trained_epochs, max_trained_epochs
        ):
            break
