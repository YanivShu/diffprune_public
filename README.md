# README #

### What is this repository for? ###

* This repository provides an implementation of the DiffPrune method suggested in the paper "DiffPrune: Neural Network 
Pruning with Deterministic Approximate Binary Gates and L<sub>0</sub> Regularization" available at 
http://arxiv.org/abs/2012.03653.
* The code enables running the two experiments performed in the paper. The first is the 
MNIST classification task using the LeNet5 model, and the second experiment is the CIFAR10 classification using a Wide
Residual Network.
* This repository is NOT maintained and is provided as is for the purpose of recreating the results reported in the 
paper.

### How do I get set up? ###
* Install the packages specified in requirements.txt using your favourite package manager.
* Run the *_demo.py scripts.
* Datasets are automatically downloaded and installed locally therefore an internet connection is required.
* Artefacts are written to the out/ directory and can be visualised using TensorBoard.

### Contribution guidelines ###
* If you notice any issue or have any feedback please email to the address specified in the paper 
http://arxiv.org/abs/2012.03653.