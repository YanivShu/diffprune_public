from pathlib import Path
from typing import List, Dict

import tensorflow as tf

from data.common import load_cifar10
from models import WideResidualNetworkDiffPrune, Config
from run import train_and_test, Sgdw

tests_dir: Path = Path(__file__).parent.joinpath(
    "out", "wide_resnet_activation_prune_cifar10"
)


def main() -> None:
    batch_size: int = 120

    tests: Dict[str, Config] = dict(
        orig=Config(
            prune=[False] * 12,
            use_regularizer=[False] * 12,
            sigmoid_gates=[True] * 12,
            lambda_=[0.0] * 12,
            eta=[None] * 12,
            train_eta=[False] * 12,
        ),
        l0_lambda_no_dropout_all_convs_pruned_sigmoid_low_compression=Config(
            prune=[True] * 12,
            use_regularizer=[True] * 12,
            sigmoid_gates=[True] * 12,
            lambda_=[1e-5] * 4 + [1e-5] * 4 + [5e-5] * 4,
            eta=[None] * 12,
            train_eta=[False] * 12,
        ),
        l0_lambda_no_dropout_all_convs_pruned_sigmoid_high_compression=Config(
            prune=[True] * 12,
            use_regularizer=[True] * 12,
            sigmoid_gates=[True] * 12,
            lambda_=[2e-5] * 4 + [7e-5] * 4 + [3e-4] * 4,
            eta=[None] * 12,
            train_eta=[False] * 12,
        ),
    )

    suite: List[str] = [k for k in tests.keys()]

    num_epochs: int = 201

    for test in suite:
        conf: Config = tests[test]
        tf.keras.backend.set_floatx(str(conf.dtype).split("'")[1])
        ds_train, ds_test, ds_info = load_cifar10(
            batch_size=batch_size, dtype=conf.dtype
        )

        model_dir: Path = tests_dir.joinpath(test)
        print(f"Starting test {test}")

        model: WideResidualNetworkDiffPrune = WideResidualNetworkDiffPrune(
            num_classes=10,
            depth=28,
            width=10,
            prune=conf.prune,
            use_regularizer=conf.use_regularizer,
            sigmoid_gates=conf.sigmoid_gates,
            lambda_=conf.lambda_,
            eta=conf.eta,
            train_eta=conf.train_eta,
            dtype=conf.dtype,
        )
        steps_per_epoch: int = ds_info.splits["train"].num_examples // batch_size + 1
        boundaries: List[int] = [steps_per_epoch * s for s in [60, 120, 160]]
        learning_rates: List[float] = [0.1 * 0.2 ** i for i in range(4)]
        weight_decays: List[float] = [0.0005 * 0.2 ** i for i in range(4)]

        learning_rate_fn: tf.keras.optimizers.schedules.PiecewiseConstantDecay = (
            tf.keras.optimizers.schedules.PiecewiseConstantDecay(
                boundaries=boundaries, values=learning_rates
            )
        )

        weight_decay_fn: tf.keras.optimizers.schedules.PiecewiseConstantDecay = (
            tf.keras.optimizers.schedules.PiecewiseConstantDecay(
                boundaries=boundaries, values=weight_decays
            )
        )

        train_and_test(
            model=model,
            train_dataset=ds_train,
            test_dataset=ds_test,
            optimizer=Sgdw(
                weight_decay=weight_decay_fn,
                learning_rate=learning_rate_fn,
                momentum=0.9,
                nesterov=True,
            ),
            num_epochs=num_epochs,
            model_dir=model_dir,
            summary_steps=50,
            max_trained_epochs=num_epochs,
            max_to_keep=20,
        )

        print(f"Finished test {test}")
        tf.keras.backend.clear_session()


if __name__ == "__main__":
    tf.keras.backend.set_floatx("float32")
    tf.compat.v1.enable_eager_execution()
    tf.config.experimental_run_functions_eagerly(run_eagerly=True)
    main()
